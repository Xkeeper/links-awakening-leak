


;===================================
;=	特殊効果		   =
;===================================
EXMAIN
	PUSH	AF
;
	LD	A,(SCRLFG)
	AND	A
	JR	NZ,EXM008
;
	LD	HL,EXTIM0
	ADD	HL,BC
	LD	A,(HL)
	AND	A
	JR	Z,EXM010
	DEC	A
	LD	(HL),A
	LD	(WORK0),A
	JR	NZ,EXM010
;
EXM008
	CALL	EXCLER
EXM010
	POP	AF
	DEC	A
	RST	0
	DW	WATRUP	; 水しぶき
	DW	SMOKE1	; 煙
	DW	SMOKET	; 煙＋宝箱
	DW	SMOKEK	; 煙＋階段
	DW	FLASH	; 火花
	DW	RENOZNZ	; 回転砲台ビーム残像
	DW	KNPWFL  ; 剣パワーアップデモ火花
	DW	HNSMOK  ; 敵跳ね返り煙
	DW	KYOPEN1	; ダンジョン入り口処理
	DW	BFZNZO	; Ｌｖ.８ 火ボス残像
	DW	DSZNZO	; プレイヤーダッシュ残像
	DW	WATRUP2	; 浅瀬用水しぶき
	DW	KBZANZ	;剣ビーム残像！
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
