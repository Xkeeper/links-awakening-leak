;
M228;¥
M229; ¥
M22A;  ¥
M22B;   ¥ NOT USED AT ALL
M22C;   /
M22D;  /
M22E; /
M22F;/
M230;   Tale's Keyhole
;       
        DB      'Une serrure ici?'
        DB      'Hum, voyons :   '
        DB      'Serrure Flagello'
        DB      WED
;
M231
;
        DB      'Une serrure ici?'
        DB      'Hum, voyons :   '
        DB      'Serrure Cyclope'
        DB      WED
;
M232        
;
        DB      'Une serrure ici?'
        DB      'Hum, voyons :   '
        DB      'Serrure Poisson'
        DB      WED
;
M233
;
        DB      'Une serrure ici?'
        DB      'Hum, voyons :   '
        DB      'Serrure Vautour'
        DB      WED
;
M234
;
        DB      'Une serrure ici?'
        DB      'Hum, voyons :   '
        DB      'Serrure Masque'
        DB      WED
;
M235;   MARIN at suspension bridge
;
        DB      'Au secours!!'
        DB      WED
;
M236;   She sees player
;
        DB      'H* #####!       '
        DB      'Des monstres    '
        DB      'horribles       '
        DB      'm^ont pos*e ici!'
        DB      'J^ai peur car   '
        DB      'j^ai le vertige.'
        DB      WED
;
M237;   When Link rescues her
;
        DB      'J^ai eu peur... '
        DB      '#####,          '
        DB      'merci!'
        DB      WED
;
M238; continued
;
        DB      '... ... ... ... '
        DB      '... ... ... ... '
        DB      '#####, euh...'
        DB      WED
;
M239; continued
;
        DB      'Euh, voyons...  '
        DB      'Comment te dire '
        DB      '... tu sais...'
        DB      WED
;
M23A;   Tarin suddenly appears.
;
        DB      'Hol%, Tarkin... '
        DB      '... ... ... ... '
        DB      'Non non, rien!  '
        DB      'Je dois partir.'
        DB      WED
;
M23B; Tarin calling for Marin
;
        DB      'MAAAARIIIIINE!!'
        DB      WED
;
M23C;¥
M23D; ¥ NOT USED
M23E; / AT ALL
M23F;/
M240;   MORE ULRIRA OLD PAL, Waterfall hint
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'D^apr+s Mamie   '
        DB      'Youpi, quelque  '
        DB      'chose est cach* '
        DB      'derri+re        '
        DB      'la Cascade de   ' 
        DB      'la Cordill+re.  '
        DB      'Ce renseignement'
        DB      't^est utile?    '
        DB      'Salut! CLIC!'
        DB      WED
;
M241;   Ulrira about tower
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'De mon temps,   '
        DB      'il y avait      '
        DB      'une Tour dans   '
        DB      'la Cordill+re.  '
        DB      'Tu devrais aller'
        DB      'y faire un tour!'
        DB      'Salut! CLIC!'
        DB      WED
;
M242; Holy Egg
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Dirige-toi vers '
        DB      'l^Oeuf Sacr*,   '
        DB      'nich* en haut   '
        DB      'du Mt Tamaranch.'
        DB      '#####,          '
        DB      'sois prudent.   '
        DB      'Salut! CLIC!'
;
        DB      WED
;
M243;   Ulrira on the passageway in the egg
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Tu t^es perdu   '
        DB      'dans l^Oeuf?    '
        DB      'D*sol*, mais    '
        DB      'je ne peux pas  '
        DB      't^aider.!       '
        DB      'Va donc te      '
        DB      'renseigner %    '
        DB      'la Biblioth+que.'
        DB      'D*sol* hein!    '
        DB      'Salut! CLIC!'
;
        DB      WED
;
M244;   Ulrira about Wanwan
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Tu devrais      '
        DB      'ramener Toutou %'
        DB      'Mme Miaou Miaou.'
        DB      'Elle sera folle '
        DB      'de joie.        '
        DB      'Salut! CLIC!'
;
        DB      WED
;
M245; Ulrira about the frog
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Tu aimerais     '
        DB      'chanter?        '
        DB      'Va voir Wart    '
        DB      'la Grenouille.  '
        DB      'Il habite dans  '
        DB      'le D*dale       '
        DB      'des Panneaux.   '
        DB      'Il est bon mais '
        DB      'un peu cher...  '
        DB      'Salut! CLIC!'
;
        DB      WED
;
M246; Ulrira Hen house
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Tu te d*fends   '
        DB      'tr+s bien!      '
        DB      'As-tu visit*    '
        DB      'le Poulailler,  '
        DB      'en haut de      '
        DB      'la montagne?    '
        DB      'A c]t*, il y a  '
        DB      'une cave qui    '
        DB      'renferme un     '
        DB      'objet important.'
        DB      'Salut! CLIC!'
;
        DB      WED
;
M247;   Ulrira, seashells
;
        DB      'DRING DRING!!   '
        DB      'P*p* le Ramollo '
        DB      '% l^appareil... '
        DB      'Tes ennemis     '
        DB      'sont coriaces?  '
        DB      'Collectionne    '
        DB      'donc tous       '
        DB      'les Coquillages.'
        DB      'Sois confiant!  '
        DB      'Bonne chance!   '
        DB      'Salut! CLIC!'
        DB      WED
;
M248; Ulrira, last
;
        DB      'DRING DRING!!   '
        DB      'Oui c^est P*p*! '
        DB      'Courage, encore '
        DB      'un effort...    '
        DB      'Je suis de tout '
        DB      'coeur avec toi. '
        DB      'Vas-y mon gars!'
        DB      WED
;
M249;   not used
M24A;   Little animal kids Rabbits #1  Before Marin
;
        DB      'Les Dunes       '
        DB      'de Yarna?       '
        DB      'Leur entr*e     '
        DB      'est au Sud,     '
        DB      'mais tu ne peux '
        DB      'pas y aller ',$AD,'   '
        DB      'Une bestiole    '
        DB      'est affal*e de  '
        DB      'tout son long!'
        DB      WED
;
M24B;   bored little rabbit
;
        DB      'Une chanson de  '
        DB      'Marine par un   '
        DB      'si beau jour, ce'
        DB      'serait le pied!'
        DB      WED
;
M24C;
;
        DB      'Savais-tu que   '
        DB      'les Villages    '
        DB      'des Animaux     '
        DB      'et des Mouettes '
        DB      '*taient des     '
        DB      'villes jumel*es?'
        DB      'Des villes,     '
        DB      'c^est un bien   '
        DB      'grand mot...    '
        DB      'Villages jumeaux'
        DB      'serait mieux!   '
        DB      'D^apr+s une     '
        DB      'source s¥re,    '
        DB      '% l^int*rieur du'
        DB      'G/te des R<ves, '
        DB      'Il y a un...    '
        DB      'Crois-tu que    '
        DB      'c^est vrai?'
        DB      WED
;
M24D; rabbit kid
;
        DB      'Il y a tellement'
        DB      'de monstres     '
        DB      'qu^on ne        '
        DB      'peut plus aller '
        DB      'au Village      '
        DB      'des Mouettes... '
        DB      'Marine me manque'
        DB      'tellement!!'
        DB      WED
;
M24E;   rabbit kid
;
        DB      'Connais-tu le   '
        DB      'Coq Volant?     '
        DB      'Autrefois,      '
        DB      'il aurait v*cu  '
        DB      'dans le Village '
        DB      'des Mouettes.   '
        DB      'Je me demande   '
        DB      'si c^est vrai!'
        DB      WED
;
M24F;   rabbit
;
        DB      'Vivement que    '
        DB      'Marine revienne!'
        DB      'Sa chanson est  '
        DB      'trop g*niale!'
        DB      WED
;
M250;   rabbit
;
        DB      'Cette nuit, j^ai'
        DB      'r<v* que j^*tais'
        DB      'une carotte.    '
        DB      'Dr]le de r<ve   '
        DB      'pour un lapin...'
        DB      WED
;
M251; rabbit
;
        DB      'Pourquoi les    '
        DB      'animaux parlent?'
        DB      'Je ne sais pas! '
        DB      'Je ne suis      '
        DB      'qu^un lapin moi!'
        DB      WED
;
M252;   When you have Marin, all rabbits say:
;       
        DB      'Oui c^est elle! '
        DB      'C^est Marine!   '
        DB      WED
;
M253;¥
M254; ¥
M255;  ¥
M256;   ¥
M257;    ¥
M258;     ¥
M259;      ¥
M25A;       ¥
M25B;        ¥
M25C;         ¥
M25D;          ¥
M25E;           ¥
M25F;            ¥
M260;             ¥
M261;              ¥
M262;               ¥
M263;                ¥
M264;                 NOT USED!!
M265;                /  
M266;               /   
M267;              /    
M268;             /     
M269;            /      
M26A;           /       
M26B;          /        
M26C;         /         
M26D;        /
M26E;       /
M26F;      /
M270;     /
M271;    /
M272;   /
M273;  /
M274; /
M275;/
;======================================================================
BANK09  GROUP   $09
        ORG     $7D00
;======================================================================
M276;   MARIN OPTION  sometimes when you hit chicken
;
        DB      'Youpi! Vas-y!   '
        DB      'Encore...       '
        DB      'Pardon?         '
        DB      'Mais non! Je    '
        DB      'n^ai rien dit.  '
        DB      'Je plaisantais!'
        DB      WED
;
M277;   MARIN OPTION when you try Ocarina w/o learning song
;
        DB      'Pas terrible... '
        DB      'Pardon? J^ai dit'
        DB      'quelque chose?  '
        DB      'H*, _a suffit   '
        DB      'la parano...'
        DB      WED
;
M278;   When you open drawers with Marin Option
;
        DB      '#####, tu       '
        DB      'fouilles souvent'
        DB      'dans les tiroirs'
        DB      'des gens?'
        DB      WED
;
M279;  When you dig with Marin
;
        DB      'Vas-y!!         '
        DB      'Creuse...       '
        DB      'Creuse encore!'
        DB      WED
;
M27A;    When you jump in a hole with Marin Option (get out of way)
;
        DB      'Ouah, super!    '
        DB      'Quelle surprise!'
        DB      WED
;
M27B;   If you don't get out of the way
;
        DB      'Oh d*sol*e!     '
        DB      'Rien de cass*   '
        DB      '#####?'
        DB      WED
;
M27C;   Tony Harman
;
        DB      'Yo, t^es qu*blo!'
        DB      WED
;
M27D;   
        DB      'Il est 9h59,    '
        DB      '_a dissuade...'
        DB      WED
;
M27E;
;
        DB      'Les nanas       '
        DB      'de la Hotline,  '
        DB      'c^est des pros!'
        DB      WED
;
M27F;
;
        DB      'Gros bisous     '
        DB      'de Kyoto!       '
        DB      '       VERO ',$CF
        DB      WED
;====================================================================
        END

