;-------------------------------------
;************************************************
;*	プログラム スタート			*
;************************************************

	ORG	$150

RESET
	CALL	LCDC_OFF	; ＬＣＤＣ ストップ
;
	LD	SP,STACK	; スタック ロケーション 変更
;
	XOR	A
	LD	(BGP),A
	LD	(OBP0),A
	LD	(OBP1),A
;
	LD	HL,CHRRAM
	LD	BC,$1800
	CALL	RMCLSB		; Character All clear !!
;
	CALL	V_RAMC		; Ｖ−ＲＡＭ クリア
;
	CALL	RAMCLR		; RAM clear !
;
	LD	A,$01
	LD	($2100),A
	CALL	DMASET		; OBJ ＤＭＡ プログラムをＲＡＭに展開
	CALL	DMASUB          ; OBJ DMA SUB (0FFC0H)
;
	CALL	DSPINT		; PPU REG. INITIAL
;
	CALL	BANKST		; CHARACTER TR.
;
	LD	A,%01000100
	LD	(STAT),A
	LD	A,$4F
	LD	(LYC),A		; Part scroll line set !
	LD	A,$01
	LD	(PBANK),A
;- - - - Play initial - - -
	LD	A,%00000001	; V BLANK only (& LCDC)  割り込み 許可
	LD	(IE),A
;
	LD	A,$01
	LD	($2100),A
	CALL	NAMESET		; CHECK SUB & SAVE DATA INITIAL
;
	LD	A,$1F
	LD	($2100),A
	CALL	SOUNDIT		; SOUND INITIAL 
;
	LD	A,$18
	LD	(OPKYTM),A	; Opening Logo display wait !
;
	EI			; 割り込み イネーブル
;
	JP	MAIN00
;****************************************
;*       ＭＡＩＮ ＰＲＯＧＲＡＭ        *
;****************************************
MAIN
	LD	A,$1
	LD	(NMIFG),A
;---- Normal game ---

      GAME PROG.

	  |
          v
;===================================================
LCDC_OFF
	LD	A,(IE)
	LD	(IEB),A		; 割り込み許可フラグ退避
	RES	0,A		; 垂直ブランク割り込み禁止
LCDC_OF10
	LD	A,(LY)		; Ｖ−ＢＬＡＮＫ 待ち
	CP	145
	JR	NZ,LCDC_OF10

	LD	A,(LCDC)	; ＬＣＤＣ ストップ
	AND	%01111111
	LD	(LCDC),A

	LD	A,(IEB)
	LD	(IE),A		; 割り込み許可フラグ復帰
	RET
;===================================================
V_RAMC
	LD	A,SPACE
	LD	BC,$800
VRC0
	LD	D,A
;
	LD	HL,$9800	; 9800-?<<SPACE
VRC1	LD	A,D    ;SPACE
	LD	(HLI),A
	DEC	BC
	LD	A,B
	OR	C
	JR	NZ,VRC1
;
	RET
;===================================================
RAMCLR
	LD	BC,$FFFD-$FF90
RMCL80
	LD	HL,$FF90
	CALL	OBJ_CLR_30	; FF page clear !
;
	LD	BC,$1F00 ; ALL WORK RAM CLEAR !
RMCL10
	LD	HL,$C000 ;OBJRAM ; 0C000H for 0CEFFH (CF00.. STACK)
OBJ_CLR_30
RMCLSB
	XOR	A
	LD	(HLI),A
	DEC	BC
	LD	A,B
	OR	C
	JR	NZ,RMCLSB
	RET
;****************************************
;*      Character bank set   　     	*
;****************************************
BANKST
	LD	A,$C
	CALL	PBSET
;
	LD	HL,CHRDAT	; 常ちゆうキャラクタ データ
	LD	DE,CHRRAM	; キャラクタ ＲＡＭ ($8000 - $97FF)
	LD	BC,$0400	; 個数 
	CALL	DATA_MOV	; キャラクタ 転送
;
	LD	A,$C
	CALL	PBSET
;
	LD	HL,CHRDAT+$0800	; ＢＧ＆ＩＴＥＭ　キャラクタ データ
	LD	DE,CHRRAM+$0800	; キャラクタ ＲＡＭ ($8000 - $97FF)
	LD	BC,$1000	; 個数 
	CALL	DATA_MOV	; キャラクタ 転送
;
	LD	HL,CHRDAT+$07A0 ;SENDT
	LD	DE,CHRRAM+$E00
	LD	BC,$0020
	CALL	DATA_MOV	; キャラクタ 転送
;
	LD	A,$01
	CALL	PBSET
;
	RET
;======================================
PBSET
	LD	($2100),A
	LD	(PBANK),A
	RET
;========================================
DSPINT
	LD	A,%11000111	; ＯＢＪ ＢＧ ＯＮ ＬＣＤＣ スタート
	LD	(LCDC),A
	LD	(LCDCB),A
	LD	A,$07
	LD	(WDX),A
	LD	A,$80
	LD	(WNDYPS),A	; Window y pos set !
	LD	(WDY),A
	LD	A,$07
	LD	(SLEVEL),A	; sound level 1 MAX set !
	LD	A,$70
	LD	(SLEVEL2),A	; sound level 2 MAX set !
TSV3010
	RET
;--------------------------------------------------------------------
DMASET
	LD	C,$C0	; DMASUB=FFC0H
	LD	B,10		; DMADATA  １０バイト分
	LD	HL,DMADATA
L2	LD	A,(HLI)
	LD	(C),A
	INC	C
	DEC	B
	JR	NZ,L2
	RET
;- - - - - - - - - - - - - - - - - - - -
DMADATA				;       DMASUB の内容
	DB	$3E,$C0		;	LD	A,LABEL2
	DB	$E0,$46		;	LD	(DMA),A
	DB	$3E,$28		;	LD	A,40	; 160サイクル
	DB	$3D		; L1	DEC	A	;    ウエイト
	DB	$20,$FD		;	JR	NZ,L1
	DB	$C9		;	RET
;=======================================================
NAMESET
;- - Have item initial - - - -
	LD	DE,$0000
	CALL	SAVECK
	LD	DE,SAVED2-SAVEDT
	CALL	SAVECK
	LD	DE,SAVED3-SAVEDT
	CALL	SAVECK
;
NMSRET
	RET
;====Save data check =========
SAVECK
	LD	C,$01
	LD	B,$05
;
	LD	HL,SAVED1
	ADD	HL,DE	
SVCK10
	CALL	SVOPEN	;(SV)
	LD	A,(HLI)
	CP	C
	JR	NZ,SVCK20
;
	INC	C
	DEC	B
	JR	NZ,SVCK10	
;
	JR	SVCK50
SVCK20
	LD	HL,SAVED1
	ADD	HL,DE
	LD	A,$01
SVCK30
	CALL	SVOPEN	;(SV)
	LD	(HLI),A
	INC	A
	CP	$06
	JR	NZ,SVCK30
;
	LD	DE,$100+$200+$7F+$01
SVCK40
	CALL	SVOPEN	;(SV)
	XOR	A
	LD	(HLI),A
	DEC	DE
	LD	A,E
	OR	D
	JR	NZ,SVCK40
;
SVCK50
	RET
