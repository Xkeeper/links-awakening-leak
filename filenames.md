Original filenames to new filenames
===================================

The files here were renamed from their old filenames to new files. May be changed later too, though.

| Description | Old filename | New filename |
| ----------- | ------------ | ------------ |
| Sound (BGM 1) | bgm_1.hex | data/sound/bgm_1.bin |
| Sound (BGM 2) | bgm_2.hex | data/sound/bgm_2.bin |
| Message data | gbmsdt.asm | src/messages.asm |
| Sound (SE) | se.hex | data/sound/se.bin |
| BG character unit data, bank data | zbd.asm | src/maps/overworld_tilesets.asm |
| BG character unit number | zbn.asm | src/defines/tilesets.asm |
| Resident Subroutine 1 | zco.asm | src/misc_1.asm |
| Resident Subroutine 2 | zbs.asm | src/misc_2.asm |
| Dungeon BG data | zdb.asm | src/maps/indoors.asm |
| Enemy 1 | zen.asm | src/enemy/enemy_1.asm |
| Enemy 2 | ze2.asm | src/enemy/enemy_2.asm |
| Enemy 3 | ze3.asm | src/enemy/enemy_3.asm |
| Enemy 4 | ze4.asm | src/enemy/enemy_4.asm |
| Enemy 5 | ze5.asm | src/enemy/enemy_5.asm |
| Enemy 6 | ze6.asm | src/enemy/enemy_6.asm |
| Enemy 7 | ze7.asm | src/enemy/enemy_7.asm |
| Enemy 8 | ze8.asm | src/enemy/enemy_8.asm |
| Enemy set data | zed.asm | src/maps/enemies.asm |
| Ending | zend.asm | src/ending.asm |
| Enemy Main | zes.asm | src/enemy/main.asm |
| Extra Subroutine | zex.asm | src/misc_3.asm |
| Ground BG data address | zgb.asm | src/maps/overworld_pointers.asm |
| Ground BG data | zgd.asm | src/maps/overworld.asm |
| Main control | zma.asm | src/main.asm |
| Message control data | zms.asm | src/defines/messages.asm |
| Player Control | zpl.asm | src/player.asm |
| RAM registration | zram.asm | src/defines/ram.asm |
| Title 2 etc | zs1.asm | src/title_menu_2.asm |
| Sound control subroutine | zsd.asm | src/sound.asm |
| Title 1 etc | zti.asm | src/title_menu_1.asm |
| Demo BG VRAM data | zvd.asm | src/ending_vram.asm |
| Registration data | zrom.asm | src/defines/header.asm |
