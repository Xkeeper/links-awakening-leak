
(Danjyon.OR.Grand : NXGO No. : D(G) Room No. : Room No For U.D.L.R)

Ｙについての補足説明
	地上の入口が
	上(Ｕ)下(Ｄ)左(Ｌ)右(Ｒ)側にある事を意味する｡

			全洞窟部屋一覧

Ｎｏ.|   座標   |  コメント
;
DB0FC(G:  :38: );うるりらの秘密部屋１：(柴原)
DB0FD(G:  :78: );うるりらの秘密部屋２：(鶏)
DB0AC(G:  :8D: );妖精の泉洞窟
;
DB0E0(G:  :E7: );小悪魔んの井戸１
DB0E1(G:  :52: );小悪魔んの井戸２
DB0E2(G:  :04: );小悪魔んの井戸３
DB0E3(G:  :F6: );オバケさんの家(オプションオバケ)
DB0E4( :  :  : );風見鳥洞窟(上)
DB0E5(G:  :E7: );こあくまん１($0E0)へ続く洞窟１(左)
DB0E6( :  :  : );こあくまん１($0E0)へ続く洞窟２(右)
DB0E7( :  :  : );ジャングル＊クルーズ復帰洞窟(上)
DB0E9(G:  :2B: );Ｌｖ．４脱出用洞窟(左)
DB0EA(G:  :2D: );Ｌｖ．４脱出用洞窟(右)
DB0EB(G:  :49: );城侵入用横画面(左)
DB0EC(G:  :4A: );城侵入用横画面(右)
;
DB0EE(G:  :03: );Ｌｖ．８へ通じる洞窟(上)
DB0EF(D:03:66: );魚ボス用横(縦)画面(上)
DB0F0(G:  :9C: );神殿に続く洞窟(左)
DB0F1(G:  :9D: );神殿に続く洞窟(右)
DB0F2(G:  :2E: );＄０７近くの穴から落下してくる。
DB0F3(G:  :87: );妖精の泉洞窟
DB0F4(G:  :92: );風見鳥洞窟(下)
DB0F5(G:  :F3: );氷見津,氷見津,秘密の部屋
DB0F6(G:  :F9: );こあくまん１($0E0)へ続く洞窟３(下)
DB0F7( :  :  : );ジャングル＊クルーズ復帰洞窟(下)
DB0F8( :  :  : );ラネモーラ落下洞窟(左)
DB0F9(G:  :CF: );ラネモーラ落下洞窟(右)
DB0FA(G:  :B0: );有害図書館
DB0FB(G:  :1F: );妖精の泉洞窟２
DB0FE(G:  :13: );Ｌｖ．８へ通じる洞窟(下)
DB0FF( :  :  : );魚ボス用横(縦)画面(下)
;
DB16F( :  :  : );巨大アモス像用部屋(上)
DB17F( :  :  : );巨大アモス像用部屋(中)
DB18F(G:  :AC: );巨大アモス像用部屋(下)
;
DB180(G:  :1E:L);タマランチ山大洞窟＄１Ｅ左の入口用１
DB181( :  :  : );タマランチ山大洞窟＄１Ｅ左の入口用２
DB182(G:  :1F:L);タマランチ山大洞窟＄１Ｅ左の入口用３
DB183(G:  :1E:R);タマランチ山大洞窟＄１Ｅ右の入口用１
DB184( :  :  : );タマランチ山大洞窟＄１Ｅ右の入口用２
DB185( :  :  : );タマランチ山大洞窟＄１Ｆ真ん中入口用１
DB186( :  :  : );タマランチ山大洞窟＄１Ｆ真ん中入口用２
DB187(G:  :1F:D);タマランチ山大洞窟＄１Ｆ真ん中入口用３
DB188( :  :  : );タマランチ山大洞窟＄１９穴入口(上)
DB189(G:  :19: );タマランチ山大洞窟＄１９穴入口(下)
DB18A( :  :  : );タマランチ山大洞窟＄０Ａ穴入口(上)
DB18B(G:  :0A:D);タマランチ山大洞窟＄０Ａ穴入口(下)
DB18C(G:  :1F:R);タマランチ山大洞窟＄１Ｆ＄０７入口(下)
DB18D( :  :  : );タマランチ山大洞窟＄１Ｆ＄０７入口(上)
DB18E(G:  :0F: );タマランチ山大洞窟＄０Ｆ＄入口(右)
DB190( :  :  : );人魚斜め上にある洞窟(＄Ｂ８上から下ってくる部屋)
DB191( :  :  : );人魚斜め上にある洞窟(＄Ｂ８と＄Ｃ８をつなぐ部屋)
DB192(G:  :B8:D);人魚斜め上にある洞窟(＄Ｂ８側)
DB193( :  :  : );人魚斜め上にある洞窟(＄Ｃ８側)
DB194(G:  :C8: );人魚斜め上にある洞窟(＄Ｂ８上左側)
DB195( :  :  : );人魚斜め上にある洞窟(＄Ｂ８上中央側)
DB196(G:  :B8:U);人魚斜め上にある洞窟(＄Ｂ８上右側)
DB197( :  :  : );人魚の像下洞窟(上)
DB198(G:  :E9: );人魚の像下洞窟(下)
DB199(G:  :11: );電話ボックス５(カメ岩付近)
DB19A( :  :  : );タマランチ山大洞窟＄０Ｆ＄入口(左)
DB19B(G:  :31: );電話ボックス６(ワキーガ沼入口)
DB19C(G:  :88: );電話ボックス７(城門前)
DB19D(G:  :E8: );電話ボックス８(人魚像側)
DB19E(G:  :B0: );図書館
DB19F(G:  :0A: );鶏小屋
;
DB1A0(G:  :B2: );ＵＦＯキャッチャー
DB1A1(G:  :93: );コーポ　Ｎｉｓｈｉｙａｍａ
DB1A2(G:  :65: );魔法のオババ
DB1A3(G:  :A2: );マリン＆タリン家(スタート)
DB1A4(G:  :A0: );井戸の中
DB1A5(G:  :82:L);サザエさんと四つ子＆パパールと五人目の子供の家１(左)
DB1A6(G:  :82:R);サザエさんと四つ子＆パパールと五人目の子供の家２(右)
DB1A7(G:  :A1: );ワンワンとマダムニャンニャンが居る家
DB1A8(G:  :30: );変な親父の家(Ｄｒ．ウイル＊ライト)
DB1A9(G:  :B1: );子供のいない夫婦の家(うるりらジジイの家)
DB1AA(G:  :83: );ヨロレヒのダンジョン(ＯＨ！カリナ入手)(旧村の宝)
DB1AB(G:  :50: );迷いの森茸の通路１
DB1AC( :  :  : );迷いの森茸の通路２
DB1AD(G:  :45: );薬屋さん(きまぐれトレーシー)
DB1AE(G:  :20: );＄２０の洞窟
DB1AF(G:  :21: );＄２１の洞窟
DB1B0(G:  :3F: );ジャングル＊クルーズ入場券販売所
DB1B1(G:  :81: );釣り堀
DB1B2(G:  :A1: );ワンワンの家(犬小屋)
DB1B3(G:  :42: );一番最後に入れる森洞窟(要パワフルブレスレット)
DB1B4(G:  :A4: );電話ボックス１(ウクク湿原)
DB1B6(G:  :17: );タマランチ山麓(ﾌﾓﾄ)＄１７(大洞窟内)
DB1B7( :  :  : );タマランチ山麓(ﾌﾓﾄ)＄１７隣部屋(大洞窟内)
DB1B8( :  :  : );タマランチ山麓(ﾌﾓﾄ)＄１７地下部屋(大洞窟内)
DB1B9( :  :  : );タマランチ山麓(ﾌﾓﾄ)＄１７地下部屋隣(大洞窟内)
DB1BA( :  :  : );タマランチ山麓(ﾌﾓﾄ)＄１８左隣部屋(大洞窟内)
DB1BB(G:  :18:L);タマランチ山麓(ﾌﾓﾄ)＄１８中央部屋(大洞窟内)
DB1BC(G:  :18:R);タマランチ山麓(ﾌﾓﾄ)＄１８右部屋(大洞窟内)
DB1BD(G:  :62: );迷いの森茸の通路３
DB1BE( :  :  : );夢の祠(左上)
DB1BF( :  :  : );夢の祠(右上)
DB1C0(G:  :D9:L);Ｌｖ．５ダンジョン侵入用横面１
DB1C1(G:  :D9:R);Ｌｖ．５ダンジョン侵入用横面２
DB1C2( :  :  : );城内部１階左上
DB1C3( :  :  : );城内部１階右上
;;;;DB1C4( :  :  : );未使用
DB1C5( :  :  : );城内部２階左上
DB1C6( :  :  : );城内部２階右上
DB1C7(G:  :D6: );貧民となったリチャード王子の家
DB1C8( :  :  : );貧民となったリチャード王子の家の地下部屋(上村ロッジ)
DB1C9(G:  :C6: );リチャード家秘密洞窟(左)
DB1CA( :  :  : );リチャード家秘密洞窟(右)
DB1CB(G:  :B3: );電話ボックス２(スタート側)
DB1CC(G:  :4B: );電話ボックス３(城)
DB1CD(G:  :84: );＄８４街の側洞窟
DB1CE( :  :  : );夢の祠(左下)
DB1CF( :  :  : );夢の祠(右下)
DB1D0(G:  :AA: );動物村へ行く地下道(左)
DB1D1(G:  :AB: );動物村へ行く地下道(右)
DB1D2( :  :  : );城内部１階左下
DB1D3(G:  :69: );城内部１階右下(＄６９)
;;;;;DB1D4( :  :  : );未使用
DB1D5(G:  :59:L);城内部２階左下(地上＄５９左)
DB1D6(G:  :59:R);城内部２階右下(地上＄５９右)
DB1D7(G:  :DD: );熊五郎の家(パイナップル専門店)
DB1D8( :  :  : );貧民となったリチャード王子の家の地下部屋(下)
DB1D9(G:  :CD:L);動物村２軒長屋左(クリスチーヌ(ただのヤギ))
DB1DA(G:  :CD:R);動物村２軒長屋右
DB1DB(G:  :CC:L);動物村一番のでけえ家左(ウサギ小屋)
DB1DC( :  :  : );動物村一番のでけえ家右(ウサギ小屋)
DB1DD(G:  :CC:R);動物村有本(＝スケベ)ワニの絵描きの家
DB1DE(G:  :75: );墓下洞窟(左)
DB1DF(G:  :76: );墓下洞窟(右)
DB1E0( :  :  : );モリブリンの住処(広場)
DB1E1( :  :  : );モリブリンの住処(モリブボスの部屋)
DB1E2( :  :  : );モリブリンの住処(牢屋:ワンワン入牢)
DB1E3(G:  :DB: );電話ボックス４(動物村付近)
DB1E4( :  :  : );＄８５爆弾壊れ洞窟上(左)
DB1E5( :  :  : );＄８５爆弾壊れ洞窟上(右)
DB1E6( :  :  : );動物村洞窟
DB1E7( :  :  : );動物村洞窟
DB1E8(D:06:F8:U);ハゲタカ用部屋(上)VOL,LV.7
DB1E9(G:  :8A: );リンクが住んでいたような気がする家(交換所)
DB1EA(G:  :15: );タマランチ山大洞窟
DB1EB( :  :  : );タマランチ山大洞窟
DB1EC( :  :  : );タマランチ山大洞窟
DB1ED( :  :  : );タマランチ山大洞窟
DB1EE(G:  :07: );タマランチ山大洞窟
DB1EF( :  :  : );タマランチ山大洞窟
;
DB1F0(G:  :36: );モリブリンの住処(入口)
DB1F1( :  :  : );タマランチ山大洞窟
DB1F2(G:  :0D: );タマランチ山大洞窟
DB1F3( :  :  : );タマランチ山大洞窟
DB1F4(G:  :85: );＄８５爆弾壊れ洞窟下
DB1F5(G:  :EA: );釣り橋下釣り吉親父がボートで優雅に釣りしている横画面
DB1F6( :  :  : );動物村洞窟
DB1F7(G:  :CD: );動物村洞窟
DB1F8(D:06:2E: );ハゲタカ用部屋(下)VOL,LV.7
DB1F9(G:  :1D:L);タマランチ山大洞窟
DB1FA(G:  :1D:R);タマランチ山大洞窟
DB1FB(G:  :D4: );マムーの隠れ家
DB1FC(G:  :AE: );葛原の下貝殻入り部屋(＄ＡＥ)
DB1FD(G:  :2A: );マンボの家(ウッ！)
DB1FE(G:  :E3: );バナナワニ園

その他余り部屋
;
;;;;;;;;;;;;;;;;;01:DB07E
;;;;;;;;;;;;;;;;;02:DB07F
03:DB0AC	;使用中！
04:DB0AD
05:DB0AE
06:DB0AF
07:DB0DE
08:DB0DF
09:DB0ED
10:DB0FC	;使用中！
11:DB0FD	;使用中！
;
12:DB12F
13:DB133
14:DB136
15:DB16C
16:DB16D
17:DB16E
18:DB177
19:DB178
20:DB179
21:DB1C4
22:DB1D4

現在電話ボックス数	８店舗

	１：ウクク平原		　＄Ａ４
	２：メーベの村		　＄Ｂ３
	３：カナレット城	　＄４Ｂ
	４：動物村		　＄ＤＢ
	５：タルタル山脈	　＄１１
	６：カナレット城門前	　＄８８
	７：ワキーガ沼侵入通路前　＄３１
	８：マーサの入江	　＄Ｅ８


　　　		　　　ハートのかけら一覧

	１：洞窟１Ａ４		井戸の中

	２：洞窟１ＡＢ		迷いの森の茸通路(茸側)

	３：洞窟１ＢＡ		タルタル山脈大洞窟中腹

	４：洞窟１ＤＦ		墓石内部(お墓側)

	５：洞窟１Ｅ５		ウクク平原爆弾壁入口洞窟秘密部屋

	６：洞窟１Ｅ６		動物村洞窟

	７：地上＄００		タルタル山脈亀岩頂上

	８：地上＄４４		コホリント平原薬屋側

	９：洞窟０Ｆ２		山の洞窟穴落ち部屋

　　　１０：地上＄７８		カナレット城堀の中潜る

　　　１１：地上＄８１		釣り屋

　　　１２：洞窟０Ｅ８		ラネモーラ下落下する


		　　　　　　貝殻一覧

　１：オバケさんの家の中	Ｄ＄０Ｅ３

　２：犬小屋内			Ｄ＄１Ｂ２

　３：岩持ち上げ		Ｇ＄００Ｃ

　４：草切り穴掘り		Ｇ＄０７４

　５：草切り			Ｇ＄０８Ｂ

　６：草切り			Ｇ＄０Ａ３

　７：木体当たり		Ｇ＄０Ａ４

　８：穴掘り			Ｇ＄０Ａ５

　９：草切り			Ｇ＄０Ａ６

１０：穴掘り			Ｇ＄０Ａ８

１１：岩持ち上げ		Ｇ＄０Ｂ９

１２：穴掘り			Ｇ＄０ＤＡ

１３：草切り			Ｇ＄０Ｅ９

１４：草切り			Ｇ＄０Ｆ８

１５：岩持ち上げ		Ｇ＄０ＦＦ


宝箱内貝殻

１６：山			Ｇ＄０１Ｄ

１７：ジャングルクルーズ	Ｇ＄０６Ｃ

１８：森			Ｇ＄０７１

１９：Ｌｖ．１			Ｄ＄００Ｃ

２０：Ｌｖ．７			Ｄ＄１０１

２１：リチャード洞窟		Ｄ＄１Ｃ８

２２：神殿隠れ洞窟		Ｄ＄１ＦＣ


追加

４／１９

２３：木に体当たり		Ｇ＄０Ｄ２

４／２２

２４：秘密の部屋		Ｇ＄０７８：Ｄ＄０ＦＤ




























