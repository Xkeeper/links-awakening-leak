




	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%	日本語版 開発スタッフ表示	      %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	DIRECTOR     	
			  TAKASHI TEZUKA  	

	DUNGEON DESIGNER 	
			  YASUHISA YAMAMURA	

	SCRIPT WRITER   	
		  	  KENSUKE TANABE  	
	  		  YOSHIAKI KOIZUMI 	

	PROGRAMMER    	
			  KAZUAKI MORITA  	
			  TAKAMITSU KUZUHARA	

	CHARACTER DESIGNER	
			  MASANAO ARIMOTO 	
			  SHIGEFUMI HINO  	

	SOUND COMPOSER  	
		          KAZUMI TOTAKA  	
 		          MINAKO HAMANO  	
		          KOZUE ISHIKAWA  	

	ILLUSTRATOR    	
		          YOUICHI KOTABE  	

	SPECIAL THANKS TO	
		          TOSHIHIKO NAKAGO 	
		    	  KEIZO KATO    	
		          KOJI KONDO    	
		 	  TOMOAKI KUROUME  	

	PRODUCER     	
		 	  SHIGERU MIYAMOTO 	
		                  	

	EXECUTIVE PRODUCER	
		 	  HIROSHI YAMAUCHI 	
